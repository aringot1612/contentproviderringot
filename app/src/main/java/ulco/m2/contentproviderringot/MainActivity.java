package ulco.m2.contentproviderringot;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import java.util.Objects;

/** Activité principale. */
public class MainActivity extends AppCompatActivity {
    private Button dictionary;
    private Button contacts;

    /** Création de l'activité. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Ajout d'un icone dans la barre d'application.
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViews();
        createEventHandlers();
    }

    /** Récupération des éléments de layout. */
    private void findViews(){
        this.dictionary = findViewById(R.id.handleDictionary);
        this.contacts = findViewById(R.id.handleContact);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        dictionary.setOnClickListener(v -> goToDictionaryActivity());
        contacts.setOnClickListener(v -> goToContactActivity());
    }

    /** Passage à l'activité de gestion d'éléments de dictionnaire. */
    private void goToDictionaryActivity() {
        Intent intent = new Intent(this, DictionaryActivity.class);
        startActivity(intent);
    }

    /** Passage à l'activité de gestion de contacts. */
    private void goToContactActivity() {
        Intent intent = new Intent(this, ContactActivity.class);
        startActivity(intent);
    }
}