package ulco.m2.contentproviderringot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.R;
import ulco.m2.contentproviderringot.beans.DictionaryElementBean;

/** Adapter personnalisé pour les élements de dictionnaire et l'affichage dans une listView. */
public class DictionaryAdapter extends ArrayAdapter<DictionaryElementBean> {
    /** Gére le changement de layout pour la suppression multiple. */
    private final boolean switchLayout;

    /** Constructeur.
     *
     * @param context Le contexte d'application.
     * @param dictionary La liste des éléments de dictionnaire à utiliser.
     * @param switchLayout Permet de changer le layout.
     */
    public DictionaryAdapter(Context context, ArrayList<DictionaryElementBean> dictionary, boolean switchLayout) {
        super(context, 0, dictionary);
        this.switchLayout = switchLayout;
    }

    /** Permet d'effectuer le rendu d'un element de listView.
     *
     * @param position La position dans la liste.
     * @param convertView La view entrante.
     * @param parent La listView.
     * @return La nouvelle view d'element.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Récupération de l'élément de dictionnaire concerné.
        DictionaryElementBean element = getItem(position);
        if (convertView == null) {
            if(switchLayout){ // Layout de suppression.
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_dictionary_element_selection, parent, false);
                // Gestion des checkBox de suppression.
                CheckBox chkBox = (CheckBox) convertView.findViewById(R.id.checkedItem);
                chkBox.setOnClickListener(v -> ((ListView) parent).setItemChecked(position,((CheckBox) v).isChecked()));
            }
            else // Layout de base.
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_dictionary_element, parent, false);
        }
        // Affichage de la valeur concernée dans le dictionnaire.
        TextView elementView = (TextView) convertView.findViewById(R.id.element);
        elementView.setText(element.getValue());
        return convertView;
    }
}