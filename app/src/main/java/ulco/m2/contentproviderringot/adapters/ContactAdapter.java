package ulco.m2.contentproviderringot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.beans.ContactBean;
import ulco.m2.contentproviderringot.R;

/** Adapter personnalisé pour les contacts et l'affichage dans une listView. */
public class ContactAdapter extends ArrayAdapter<ContactBean> {
    /** Gére le changement de layout pour la suppression multiple. */
    private final boolean switchLayout;

    /** Constructeur.
     *
     * @param context Le contexte d'application.
     * @param contacts La liste de contact à utiliser.
     * @param switchLayout Permet de changer le layout.
     */
    public ContactAdapter(Context context, ArrayList<ContactBean> contacts, boolean switchLayout) {
        super(context, 0, contacts);
        this.switchLayout = switchLayout;
    }

    /** Permet d'effectuer le rendu d'un element de listView.
     *
     * @param position La position dans la liste.
     * @param convertView La view entrante.
     * @param parent La listView.
     * @return La nouvelle view d'element.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Récupération du contact concerné.
        ContactBean contact = getItem(position);
        if (convertView == null) {
            if(switchLayout){ // Layout de suppression.
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_contact_selection, parent, false);
                // Gestion des checkBox de suppression.
                CheckBox chkBox = (CheckBox) convertView.findViewById(R.id.checkedItem);
                chkBox.setOnClickListener(v -> ((ListView) parent).setItemChecked(position,((CheckBox) v).isChecked()));
            }
            else // Layout de base.
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_contact, parent, false);
        }
        // Affichage des valeurs de contact.
        TextView fullName = (TextView) convertView.findViewById(R.id.fullName);
        TextView phoneNumber = (TextView) convertView.findViewById(R.id.phoneNumber);
        fullName.setText(contact.getFullName());
        phoneNumber.setText(contact.getPhoneNumber());
        return convertView;
    }
}