package ulco.m2.contentproviderringot;

import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.adapters.DictionaryAdapter;
import ulco.m2.contentproviderringot.beans.DictionaryElementBean;
import ulco.m2.contentproviderringot.services.DictionaryService;

/** Activité d'affichage des éléments de dictionnaire. */
public class DictionaryActivity extends AppCompatActivity {
    private ListView dictionary;
    private ArrayList<DictionaryElementBean> dictionaryList;
    private SwitchCompat alphabeticalSwitch;
    private ArrayAdapter<DictionaryElementBean> adapter;
    private LinearLayout container;
    private LinearLayout containerDelete;
    private Button add;
    private Button delete;
    private Button deleteSelected;
    private Button cancel;
    private boolean order;
    private DictionaryService dictionaryService;

    private final static int REQUEST_ADD_ELEMENT = 1;

    /** Création de l'activité. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        findViews();
        init();
        createEventHandlers();
    }

    /** Initialisation de certaines valeurs pour l'activité. */
    private void init(){
        dictionaryList = new ArrayList<>();
        dictionary.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        this.alphabeticalSwitch.setChecked(true);
        this.order = true;
        this.dictionaryService = DictionaryService.getInstance();
        dictionaryList = dictionaryService.getUserDictionary(order, this);
        setAdapter(false);
        adapter.notifyDataSetChanged();
    }

    /** Permet de gérer l'adapter selon son layout.
     *
     * @param switchLayout true si le layout doit être un layout de suppression, false sinon.
     */
    private void setAdapter(boolean switchLayout){
        adapter = new DictionaryAdapter(this, dictionaryList, switchLayout);
        ListView listView = (ListView) findViewById(R.id.dictionary);
        listView.setAdapter(adapter);
        if(!switchLayout){
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        }
    }

    /** Récupération des éléments de layout. */
    private void findViews(){
        this.dictionary = findViewById(R.id.dictionary);
        this.alphabeticalSwitch = findViewById(R.id.alphabeticalSwitch);
        this.container = findViewById(R.id.container);
        this.containerDelete = findViewById(R.id.containerDelete);
        this.add = findViewById(R.id.add);
        this.delete = findViewById(R.id.delete);
        this.deleteSelected = findViewById(R.id.deleteSelected);
        this.cancel = findViewById(R.id.cancel);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        alphabeticalSwitch.setOnCheckedChangeListener((v, i) -> switchOrder(i));
        add.setOnClickListener(v -> addElement());
        delete.setOnClickListener(v -> deleteElement());
        deleteSelected.setOnClickListener(v -> deleteSelectedElements());
        cancel.setOnClickListener(v -> cancel());
    }

    /** Permet de supprimer les éléments sélectionnés par la liste de suppression. */
    private void deleteSelectedElements() {
        ContentResolver resolver = getContentResolver();
        SparseBooleanArray checkedPositions = dictionary.getCheckedItemPositions();
        for (int i = dictionary.getCount() - 1; i >=0 ; i--) {
            if(checkedPositions.get(i))
                dictionaryService.deleteElement(dictionaryList.get(i).getValue(), this);
        }
        dictionary.clearChoices();
        dictionaryList = dictionaryService.getUserDictionary(order, this);
        adapter.notifyDataSetChanged();
        cancel();
    }

    /** Gére le retour au layout de base. */
    private void cancel() {
        this.container.setVisibility(View.VISIBLE);
        this.containerDelete.setVisibility(View.GONE);
        setAdapter(false);
    }

    /** Permet de passer à l'activité d'ajout d'élément. */
    private void addElement() {
        Intent intent = new Intent(this, AddElementInDictionaryActivity.class);
        startActivityForResult(intent, REQUEST_ADD_ELEMENT);
    }

    /** Permet de changer le layout pour pouvoir supprimer des éléments. */
    private void deleteElement() {
        setAdapter(true);
        this.container.setVisibility(View.GONE);
        this.containerDelete.setVisibility(View.VISIBLE);
    }

    /** Permet de changer l'ordre d'affichage des éléments.
     *
     * @param newValue le nouvelle ordre à utiliser.
     */
    private void switchOrder(boolean newValue){
        alphabeticalSwitch.setText((newValue ) ? getString(R.string.switchTextOn) : getString(R.string.switchTextOff));
        order = newValue;
        dictionaryList = dictionaryService.getUserDictionary(order, this);
        adapter.notifyDataSetChanged();
    }

    /** Gestion des retours d'activités secondaires. */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ADD_ELEMENT){
            dictionaryList = dictionaryService.getUserDictionary(order, this);
            adapter.notifyDataSetChanged();
        }
    }
}