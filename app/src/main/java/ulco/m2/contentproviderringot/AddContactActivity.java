package ulco.m2.contentproviderringot;

import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.beans.ContactBean;
import ulco.m2.contentproviderringot.services.ContactService;

/** Activité permettant d'ajouter un contact. */
public class AddContactActivity extends AppCompatActivity {
    private Button addButton;
    private Button cancelButton;
    private EditText firstName;
    private EditText lastName;
    private EditText phoneNumber;
    private ArrayList<String> values;
    private ArrayList<ContactBean> contactList;
    private ContactService contactService;

    /** Création de l'activité. */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        findViews();
        createEventHandlers();
        init();
    }

    /** Initialisation de certaines valeurs pour l'activité. */
    private void init(){
        this.values = new ArrayList<>();
        contactList = new ArrayList<>();
        this.contactService = ContactService.getInstance();
    }

    /** Récupération des éléments de layout. */
    private void findViews() {
        this.addButton = findViewById(R.id.addElement);
        this.cancelButton = findViewById(R.id.cancelFragment);
        this.firstName = findViewById(R.id.firstName);
        this.lastName = findViewById(R.id.lastName);
        this.phoneNumber = findViewById(R.id.phoneNumber);
    }

    /** Permet d'associer à chaque bouton son listener. */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void createEventHandlers(){
        // Fait appel au service de contact pour l'ajout.
        addButton.setOnClickListener(v -> contactService.add(lastName, firstName, phoneNumber, this));
        cancelButton.setOnClickListener(v -> handleReturn());
    }

    /** Gére le retour à l'activité principale. */
    private void handleReturn(){
        finish();
    }
}