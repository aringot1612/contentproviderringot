package ulco.m2.contentproviderringot;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.services.DictionaryService;

/** Activité permettant d'ajouter un élément de dictionnaire. */
public class AddElementInDictionaryActivity extends AppCompatActivity {
    private Button addButton;
    private Button cancelButton;
    private EditText element;
    private ArrayList<String> values;
    private DictionaryService dictionaryService;

    /** Création de l'activité. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_element_dictionary);
        findViews();
        createEventHandlers();
        init();
    }

    /** Initialisation de certaines valeurs pour l'activité. */
    private void init(){
        this.values = new ArrayList<>();
        dictionaryService = DictionaryService.getInstance();
    }

    /** Récupération des éléments de layout. */
    private void findViews() {
        this.addButton = findViewById(R.id.addElement);
        this.cancelButton = findViewById(R.id.cancelFragment);
        this.element = findViewById(R.id.element);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        // Fait appel au service de dictionnaire pour l'ajout.
        addButton.setOnClickListener(v -> dictionaryService.add(element, this));
        cancelButton.setOnClickListener(v -> handleReturn());
    }

    /** Gére le retour à l'activité principale. */
    private void handleReturn(){
        finish();
    }
}