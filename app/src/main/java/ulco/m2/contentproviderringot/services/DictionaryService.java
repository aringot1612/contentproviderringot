package ulco.m2.contentproviderringot.services;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.provider.UserDictionary;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.R;
import ulco.m2.contentproviderringot.beans.DictionaryElementBean;

/** Classe de service élément de dictionnaire (Singleton). */
public class DictionaryService {
    private static DictionaryService instance;
    private final ArrayList<DictionaryElementBean> elementList;

    /** Constructeur. */
    private DictionaryService() {
        this.elementList = new ArrayList<>();
    }

    /** Permet de récupérer le singleton. */
    public static DictionaryService getInstance() {
        if (instance == null)
            instance = new DictionaryService();
        return instance;
    }

    /** Permet d'ajouter un élément dans le dictionnaire du téléphone.
     *
     * @param element l'élément de dictionnaire à ajouter.
     * @param a L'activité à utiliser pour la gestion des permissions.
     */
    public void add(EditText element, Activity a) {
        // Récupération des éléments de dictionnaire déjà présents dans le téléphone pour vérifier les doublons.
        getUserDictionary(true, a);
        // Vérification de l'edit text.
        if(element.getText().toString().isEmpty())
            Toast.makeText(a, R.string.elementMissing, Toast.LENGTH_SHORT).show();
        // L'élément est déjà présent dans le dictionnaire.
        else if(containsElement(elementList, element.getText().toString()))
            Toast.makeText(a, R.string.elementDuplicate, Toast.LENGTH_SHORT).show();
        else{
            // Récupération du contentResolver depuis l'activité principale.
            ContentResolver resolver = a.getContentResolver();
            // Création d'un conteneur pour stocker un élément de dictionnaire.
            ContentValues values = new ContentValues();
            // Ajout d'une valeur dans le conteneur.
            values.put(UserDictionary.Words.WORD, element.getText().toString());
            // Application de la requête.
            resolver.insert(UserDictionary.Words.CONTENT_URI, values);
            Toast.makeText(a, R.string.elementAdded, Toast.LENGTH_SHORT).show();
        }
    }

    /** Permet de récupérer tous les éléments de dictionnaire existants dans le téléphone.
     *
     * @param order L'ordre à utiliser pour rendre les éléments de dictionnaire.
     * @param a L'activité à utiliser pour la gestion des permissions.
     * @return La liste des élements de dictionnaire.
     */
    @TargetApi(26)
    public ArrayList<DictionaryElementBean> getUserDictionary(boolean order, Activity a){
        // On vide les éléments de dictionnaire de la liste actuelle.
        elementList.clear();
        // Récupération du contentResolver depuis l'activité principale.
        ContentResolver resolver = a.getContentResolver();
        // Création du curseur.
        Cursor cursor;
        // Curseur selon l'ordre alphabétique de la valeur.
        if(order)
            cursor = resolver.query(UserDictionary.Words.CONTENT_URI, null, null, null, UserDictionary.Words.WORD + " ASC");
        else
            cursor = resolver.query(UserDictionary.Words.CONTENT_URI, null, null, null, UserDictionary.Words.WORD + " DESC");
        // Récupération de l'index de colonne valeur.
        int index = cursor.getColumnIndexOrThrow(UserDictionary.Words.WORD);
        // Tant qu'il existe des valeurs dans le dictionnaire...
        while(cursor.moveToNext()) {
            // Ajout de l'élément dans la liste.
            elementList.add( new DictionaryElementBean(cursor.getString(index)));
        }
        cursor.close();
        return elementList;
    }

    /** Permet de supprimer un élément de dictionnaire par valeur.
     *
     * @param value La valeur à supprimer.
     * @param a L'activité à utiliser pour la gestion des permissions.
     */
    public void deleteElement(String value, Activity a) {
        // Récupération du contentResolver depuis l'activité principale.
        ContentResolver resolver = a.getContentResolver();
        // Application d'une requête de suppression par valeur.
        resolver.delete(UserDictionary.Words.CONTENT_URI, UserDictionary.Words.WORD + "='" + value + '\'', null);
    }

    /** Permet de vérifier si une liste d'élément de dictionnaire contient un élément avec une valeur donnée.
     *
     * @param list La liste à utiliser.
     * @param value La valeur à utiliser.
     * @return true si la valeur existe, false sinon.
     */
    private boolean containsElement(final ArrayList<DictionaryElementBean> list, final String value){
        for(DictionaryElementBean contact : list){
            if(contact.getValue().equals(value))
                return true;
        }
        return false;
    }
}
