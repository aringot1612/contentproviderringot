package ulco.m2.contentproviderringot.services;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.R;
import ulco.m2.contentproviderringot.beans.ContactBean;

/** Classe de service contact (Singleton). */
public final class ContactService {
    private static ContactService instance;
    private final ArrayList<ContactBean> contactList;

    /** Constructeur. */
    private ContactService() {
        this.contactList = new ArrayList<>();
    }

    /** Permet de récupérer le singleton. */
    public static ContactService getInstance() {
        if (instance == null)
            instance = new ContactService();
        return instance;
    }

    /** Permet d'ajouter un contact dans le téléphone.
     *
     * @param lastName Le nom de famille pour le contact à enregistrer.
     * @param firstName Le prénom pour le contact à enregistrer.
     * @param phoneNumber Le numéro pour le contact à enregistrer.
     * @param a L'activité à utiliser pour la gestion des permissions.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void add(EditText lastName, EditText firstName, EditText phoneNumber, Activity a) {
        // Récupération des contacts déjà présents dans le téléphone pour vérifier les doublons.
        getUsercontacts(true, a);
        // Vérification des champs de saisie contact.
        if(!(firstName.getText().toString().isEmpty() || lastName.getText().toString().isEmpty() || phoneNumber.getText().toString().isEmpty())){
            // On vérifie si un contact est déjà présent avec ce numéro de téléphone.
            if(!containsPhoneNumber(contactList, phoneNumber.getText().toString())){
                // Vérification des permissions.
                if(isPermissionGrantedForContactsWrite(a)){
                    // Récupération du contentResolver depuis l'activité principale.
                    ContentResolver resolver = a.getContentResolver();
                    // Préparation de la requête.
                    ArrayList<ContentProviderOperation> ops = new ArrayList<>();
                    // Ajout du contact dans la table RawContacts.
                    ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                            .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                            .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
                    // Ajout du nom complet dans la table Data.
                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,firstName.getText().toString() + " " + lastName.getText().toString()).build());
                    // Ajout du nom de famille dans la table Data.
                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                            .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,lastName.getText().toString()).build());
                    // Ajout du numéro de téléphone dans la table Data.
                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                            .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,phoneNumber.getText().toString())
                            .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,phoneNumber.getText().toString()).build());
                    try {
                        // Lancement de la requête.
                        resolver.applyBatch(ContactsContract.AUTHORITY, ops);
                        Toast.makeText(a, R.string.contactAdded, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(a, R.string.contactError, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            else
                Toast.makeText(a, R.string.phoneAlreadyExist, Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(a, R.string.contactMissing, Toast.LENGTH_SHORT).show();
    }

    /** Permet de récupérer tous les contacts existants dans le téléphone.
     *
     * @param order L'ordre à utiliser pour rendre les contacts.
     * @param a L'activité à utiliser pour la gestion des permissions.
     * @return La liste de contacts.
     */
    @TargetApi(26)
    public ArrayList<ContactBean> getUsercontacts(boolean order, Activity a){
        // On vide les contacts de la liste actuelle.
        contactList.clear();
        // Vérification des permissions.
        if(isPermissionGrantedForContactsRead(a)){
            // Récupération du contentResolver depuis l'activité principale.
            ContentResolver contentResolver = a.getContentResolver();
            // Création du curseur principal.
            Cursor cursor;
            // Curseur selon l'ordre des contacts par display name.
            if(order)
                cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
            else
                cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" DESC");
            // On vérifie si des contacts sont présents dans le téléphone.
            if (cursor.getCount() > 0) {
                // Parcours des contacts.
                while (cursor.moveToNext()) {
                    // Récupération de l'id de colonne pour l'id de contact.
                    int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
                    // Récupération de l'id de colonne de la présence d'un numéro de téléphone.
                    int idPhoneNumberCheck = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                    // Récupération de l'id de colonne du nom complet.
                    int idName = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                    // Récupération de l'id de contact.
                    String id = cursor.getString(idIndex);
                    // Vérification de la présence d'un numéro de téléphone.
                    if (cursor.getInt(idPhoneNumberCheck) > 0) {
                        // Création d'un curseur pour récupérer les données du contact en utilisant l'id de contact.
                        Cursor cursorData = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        cursorData.moveToNext();
                        // Récupération de l'id de colonne du numéro de téléphone.
                        int idPhoneNumber = cursorData.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        // Vérification : la colonne existe bien.
                        if(idPhoneNumber >= 0){
                            // Récupération du numéro de téléphone.
                            String phoneNumber = cursorData.getString(idPhoneNumber);
                            // Récupération du nom complet.
                            String fullName = cursor.getString(idName);
                            // Ajout dans la liste de contacts.
                            if(!containsPhoneNumber(contactList, phoneNumber)){
                                contactList.add(new ContactBean(id, phoneNumber, fullName));
                            }
                        }
                        cursorData.close();
                    }
                }
                cursor.close();
            }
        }
        return contactList;
    }

    /** Permet de supprimer un contact par numéro de téléphone.
     *
     * @param contactHelper Le ContentResolver à utiliser.
     * @param number Le numéro du contact à supprimer.
     * @param id L'id du contact à supprimer dans le tableau de contact.
     * @param a L'activité à utiliser pour la gestion des permissions.
     */
    public void deleteContact(ContentResolver contactHelper, String number, String id, Activity a) {
        // Vérification des permissions.
        if(isPermissionGrantedForContactsWrite(a)){
            ArrayList<ContentProviderOperation> ops = new ArrayList<>();
            // Création de la requête de suppression.
            String[] args = new String[] { String.valueOf(getContactID(contactHelper, number)) };
            ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI)
                    .withSelection(ContactsContract.RawContacts.CONTACT_ID + "=?", args)
                    .build());
            try {
                // Application de la requête.
                contactHelper.applyBatch(ContactsContract.AUTHORITY, ops);
                // Suppression du contact dans la liste de contacts.
                contactList.remove(Integer.parseInt(id));
            } catch (RemoteException | OperationApplicationException e) {
                e.printStackTrace();
            }
        }
    }

    /** Permet de récupérer un id de contact dans la table de contact selon le numéro de téléphone.
     *
     * @param contactHelper Le ContentResolver à utiliser.
     * @param number Le numéro de téléphone à utiliser.
     * @return l'id du contact.
     */
    private long getContactID(ContentResolver contactHelper,String number) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String[] projection = { ContactsContract.PhoneLookup._ID };
        try (Cursor cursor = contactHelper.query(contactUri, projection, null, null, null)) {
            if (cursor.moveToFirst()) {
                int personID = cursor.getColumnIndex(ContactsContract.PhoneLookup._ID);
                return cursor.getLong(personID);
            }
            return -1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /** Permet de vérifier si une liste de contact contient un élément avec un numéro de téléphone donné.
     *
     * @param list La liste à utiliser.
     * @param phoneNumber Le numéro de téléphone à utiliser.
     * @return true si le numéro existe, false sinon.
     */
    private boolean containsPhoneNumber(final ArrayList<ContactBean> list, final String phoneNumber){
        for(ContactBean contact : list){
            if(contact.getPhoneNumber().equals(phoneNumber))
                return true;
        }
        return false;
    }

    /** Permet de gérer les permsissions en lecture des contacts.
     *
     * @param a L'activité à utiliser pour la gestion des permissions.
     * @return true si la permission a été obtenue, false sinon.
     */
    public boolean isPermissionGrantedForContactsRead(Activity a) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (a.checkSelfPermission(android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(a, new String[]{Manifest.permission.READ_CONTACTS}, 1);
                return false;
            }
        }
        else {
            return true;
        }
    }

    /** Permet de gérer les permsissions en écriture des contacts.
     *
     * @param a L'activité à utiliser pour la gestion des permissions.
     * @return true si la permission a été obtenue, false sinon.
     */
    public boolean isPermissionGrantedForContactsWrite(Activity a) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (a.checkSelfPermission(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(a, new String[]{Manifest.permission.WRITE_CONTACTS}, 2);
                return false;
            }
        }
        else {
            return true;
        }
    }
}
