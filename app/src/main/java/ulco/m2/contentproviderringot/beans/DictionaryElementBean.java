package ulco.m2.contentproviderringot.beans;

/** Bean permettant de stocker un élément de dictionnaire. */
public class DictionaryElementBean {
    private String value;

    public DictionaryElementBean(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
