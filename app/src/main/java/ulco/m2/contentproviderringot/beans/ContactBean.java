package ulco.m2.contentproviderringot.beans;

/** Bean permettant de stocker un contact. */
public class ContactBean{
    private String id;
    private final String phoneNumber;
    private final String fullName;

    public ContactBean(String id, String phoneNumber, String fullName){
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.fullName = fullName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoneNumber(){
        return this.phoneNumber;
    }

    public String getFullName(){
        return this.fullName;
    }
}