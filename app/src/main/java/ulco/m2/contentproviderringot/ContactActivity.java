package ulco.m2.contentproviderringot;

import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import java.util.ArrayList;
import ulco.m2.contentproviderringot.adapters.ContactAdapter;
import ulco.m2.contentproviderringot.beans.ContactBean;
import ulco.m2.contentproviderringot.services.ContactService;

/** Activité d'affichage des contacts. */
public class ContactActivity extends AppCompatActivity {
    private ListView contacts;
    private ArrayList<ContactBean> contactList;
    private SwitchCompat alphabeticalSwitch;
    private ArrayAdapter<ContactBean> adapter;
    private LinearLayout container;
    private LinearLayout containerDelete;
    private Button add;
    private Button delete;
    private Button deleteSelected;
    private Button cancel;
    private boolean order;
    private ContactService contactService;
    private final static int REQUEST_ADD_CONTACT = 1;

    /** Création de l'activité. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        findViews();
        init();
        createEventHandlers();
    }

    /** Initialisation de certaines valeurs pour l'activité. */
    private void init(){
        contactList = new ArrayList<>();
        contacts.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        this.alphabeticalSwitch.setChecked(true);
        this.order = true;
        this.contactService = ContactService.getInstance();
        contactList = contactService.getUsercontacts(order, this);
        setAdapter(false);
        adapter.notifyDataSetChanged();
    }

    /** Permet de gérer l'adapter selon son layout.
     *
     * @param switchLayout true si le layout doit être un layout de suppression, false sinon.
     */
    private void setAdapter(boolean switchLayout){
        adapter = new ContactAdapter(this, contactList, switchLayout);
        ListView listView = (ListView) findViewById(R.id.contacts);
        listView.setAdapter(adapter);
        if(!switchLayout){
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        }
    }

    /** Récupération des éléments de layout. */
    private void findViews(){
        this.contacts = findViewById(R.id.contacts);
        this.alphabeticalSwitch = findViewById(R.id.alphabeticalSwitch);
        this.container = findViewById(R.id.container);
        this.containerDelete = findViewById(R.id.containerDelete);
        this.add = findViewById(R.id.add);
        this.delete = findViewById(R.id.delete);
        this.deleteSelected = findViewById(R.id.deleteSelected);
        this.cancel = findViewById(R.id.cancel);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        alphabeticalSwitch.setOnCheckedChangeListener((v, i) -> switchOrder(i));
        add.setOnClickListener(v -> addElement());
        delete.setOnClickListener(v -> deleteElement());
        deleteSelected.setOnClickListener(v -> deleteSelectedElements());
        cancel.setOnClickListener(v -> cancel());
    }

    /** Permet de supprimer les éléments sélectionnés par la liste de suppression. */
    private void deleteSelectedElements() {
        ContentResolver resolver = getContentResolver();
        SparseBooleanArray checkedPositions = contacts.getCheckedItemPositions();
        for (int i = contacts.getCount() - 1; i >=0 ; i--) {
            if(checkedPositions.get(i))
                contactService.deleteContact(resolver, contactList.get(i).getPhoneNumber(), Integer.toString(i), this);
        }
        contacts.clearChoices();
        contactList = contactService.getUsercontacts(order, this);
        adapter.notifyDataSetChanged();
        cancel();
    }

    /** Gére le retour au layout de base. */
    private void cancel() {
        this.container.setVisibility(View.VISIBLE);
        this.containerDelete.setVisibility(View.GONE);
        setAdapter(false);
    }

    /** Permet de passer à l'activité d'ajout d'élément. */
    private void addElement() {
        Intent intent = new Intent(this, AddContactActivity.class);
        startActivityForResult(intent, REQUEST_ADD_CONTACT);
    }

    /** Permet de changer le layout pour pouvoir supprimer des éléments. */
    private void deleteElement() {
        setAdapter(true);
        this.container.setVisibility(View.GONE);
        this.containerDelete.setVisibility(View.VISIBLE);
    }

    /** Permet de changer l'ordre d'affichage des éléments.
     *
     * @param newValue le nouvelle ordre à utiliser.
     */
    private void switchOrder(boolean newValue){
        alphabeticalSwitch.setText((newValue ) ? getString(R.string.switchTextOn) : getString(R.string.switchTextOff));
        order = newValue;
        contactList = contactService.getUsercontacts(order, this);
        adapter.notifyDataSetChanged();
    }

    /** Gestion des retours d'activités secondaires. */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ADD_CONTACT){
            contactList = contactService.getUsercontacts(order, this);
            adapter.notifyDataSetChanged();
        }
    }
}