# ContentProviderRingot

Projet - TP : Application Android (java)

## Auteur
- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçus :

<br>

### Page principale

<br>![Aperçu : Page principale](images/home.png)

<br>

### Page principale du dictionnaire

<br>![Aperçu : Page principale du dictionnaire](images/home_dictionary.png)

<br>

### Suppression des éléments de dictionnaire

<br>![Aperçu : Suppression des éléments de dictionnaire](images/dictionary_delete.png)

<br>

### Ajout d'un élément de dictionnaire

<br>![Aperçu : Ajout d'un élément de dictionnaire](images/dictionary_add.png)

<br>

### Page principale des contacts

<br>![Aperçu : Page principale des contacts](images/home_contacts.png)

<br>

### Suppression des contacts

<br>![Aperçu : Suppression des contacts](images/contacts_delete.png)

<br>

### Ajout d'un contact

<br>![Aperçu : Ajout d'un contact](images/contacts_add.png)